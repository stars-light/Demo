package CC.Observer;

import CC.Observer.Impl.UserPeople;
import CC.Observer.Impl.WechatServer;

public class Test {

    public static void main(String[] args) {
        WechatServer server = new WechatServer();

        Subscriber userZhang = new UserPeople("ZhangSan");
        Subscriber userLi = new UserPeople("LiSi");
        Subscriber userWang = new UserPeople("WangWu");

        server.registerSubscriber(userZhang);
        server.registerSubscriber(userLi);
        server.registerSubscriber(userWang);
        server.setInfomation("PHP是世界上最好用的语言！");

        System.out.println("----------------------------------------------");
        server.removeSubscriber(userZhang);
        server.setInfomation("JAVA是世界上最好用的语言！");

    }
}
