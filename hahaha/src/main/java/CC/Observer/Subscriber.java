package CC.Observer;

//订阅者
public interface Subscriber {

    /**
     * 定义了一个update()方法，当被订阅者调用notifyObservers()方法时，
     * 订阅者的update()方法会被回调。
     * @param message
     */
    public void update(String message);

}
