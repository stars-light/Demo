package CC.Observer;

//被订阅者
public interface Publisher {

    //注册订阅者
    public void registerSubscriber(Subscriber s);
    //移除订阅者
    public void removeSubscriber(Subscriber s);
    //消息通知订阅者
    public void notifySubscriber();

}
