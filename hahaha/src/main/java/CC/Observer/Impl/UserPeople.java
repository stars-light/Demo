package CC.Observer.Impl;

import CC.Observer.Subscriber;

public class UserPeople implements Subscriber {

    private String name;
    private String message;

    public UserPeople(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        this.message = message;
        read();

    }

    public void read() {
        System.out.println(name + " 收到推送消息： " + message);
    }
}
