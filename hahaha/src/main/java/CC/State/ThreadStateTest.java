package CC.State;

import CC.State.demo2.Dead;
import CC.State.demo2.New;
import CC.State.demo2.ThreadContext;
import CC.State.demo2.ThreadState;


public class ThreadStateTest {

    public static void main(String[] args) {

        ThreadContext context = new ThreadContext();
        context.start();
        context.getCPU();
        context.suspend();
        activation(context.getState());
        context.resume();
        context.getCPU();
        context.stop();

        context.setState(activation(context.getState()));
        context.start();


    }

    //激活死亡状态
    public static New activation (ThreadState state){
        if (state instanceof Dead){
            System.out.println("----激活死亡状态程序----");
            return new New();
        }else {
            System.out.println("程序为非死亡状态");
        }

        return null;
    }
}
