package CC.State.demo3.Share;

public abstract class ShareState {
    public abstract void Handle(ShareContext context);
}
