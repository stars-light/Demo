package CC.State.demo3.Share;

//具体状态2类
public class ConcreteState2 extends ShareState{
    @Override
    public void Handle(ShareContext context) {
        System.out.println("当前状态是： 状态2");
        context.setState(context.getState("1"));
    }
}
