package CC.State.demo3.Share;

//具体状态1类
public class ConcreteState1 extends ShareState{
    @Override
    public void Handle(ShareContext context) {
        System.out.println("当前状态是： 状态1");
        context.setState(context.getState("2"));
    }
}
