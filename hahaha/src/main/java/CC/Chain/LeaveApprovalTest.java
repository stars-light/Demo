package CC.Chain;

import CC.Chain.vacate.*;

public class LeaveApprovalTest {

    public static void main(String[] args) {
        //设置请假天数，得出审批结果
        result(6);
    }

    public static void result(int LeaveDays){

        //组装责任链

        //审批人1
        Leader approver1 = new ClassAdviser();
        //审批人2
        Leader approver2 = new DepartmentHead();
        //审批人3
        Leader approver3 = new Dean();
        //审批人4
        Leader approver4 = new DeanOfStudies();
        approver1.setNext(approver2);
        approver2.setNext(approver3);
        approver3.setNext(approver4);

        //提交请求
        approver1.handleRequest(LeaveDays);
    }
}
