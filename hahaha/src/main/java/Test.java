import java.text.DecimalFormat;

public class Test {

    public static void main(String[] args) {

        String str2 = null;
        String str = "1.001";
        String str1 = "";

        String s2 = changeRate(str2);
        System.out.println(s2);

        String s = changeRate(str);
        System.out.println(s);

        String s1 = changeRate(str1);
        System.out.println(s1);

        System.out.println(getABA("0"));

        String aaa = "凭证号@终审标志@实时数据";

        String[] strings = aaa.split("@");

        for(String string : strings) {

            System.out.println(string);

        }


    }

    /**
     * 利率转换,扩大100倍
     */
    public static String changeRate(String str){

        if(str == null || str == ""){
            return "0.0";
        }else{
            Double rate = Double.parseDouble(str);
            double d = (double)rate*100;
            DecimalFormat df = new DecimalFormat("###0.######");
            String rateResult = df.format(d);
            return rateResult;
        }

    }

    /**
     * 获取ABA标识,票据是否可选
     * @param str
     * @return
     */
    public static String getABA(String str){

        if(str == null || str == ""){
            return "-1";
        }else if("0".equals(str)){
            return "0";
        }else if("1".equals(str)){
            return "1";
        }else {
            return "-1";
        }
    }



}
