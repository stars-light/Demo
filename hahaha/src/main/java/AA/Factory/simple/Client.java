package AA.Factory.simple;

public class Client {
    public static void main(String[] args) {


        //内部类可以直接使用类，不需要new实例化对象
        Product product = SimpleFactory.makeProduct(2);
        //断言，当表达式判断为真时继续向下运行程序，否则抛出AssertionError
        assert product != null;
        product.show();




    }

    //抽象产品
    public interface Product {
        void show();
    }
    //具体产品：ProductA
    static class ConcreteProduct1 implements Product {
        public void show() {
            System.out.println("具体产品1显示...");
        }
    }
    //具体产品：ProductB
    static class ConcreteProduct2 implements Product {
        public void show() {
            System.out.println("具体产品2显示...");
        }
    }

    //具体产品：ProductC
    static class ConcreteProduct3 implements Product {
        public void show() {
            System.out.println("具体产品3显示...");
        }
    }
    final class Const {
        static final int PRODUCT_A = 0;
        static final int PRODUCT_B = 1;
        static final int PRODUCT_C = 2;
    }
    static class SimpleFactory {
        public static Product makeProduct(int kind) {
            switch (kind) {
                case Const.PRODUCT_A:
                    return new ConcreteProduct1();
                case Const.PRODUCT_B:
                    return new ConcreteProduct2();
                case Const.PRODUCT_C:
                    return new ConcreteProduct3();
            }
            return null;
        }
    }
}
